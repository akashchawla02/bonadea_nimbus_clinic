'use strict';

/**
 * Module dependencies.
 */
var should    = require('should'),
    mongoose  = require('mongoose'),
    User      = mongoose.model('User'),
    constants = require('../../utility/constants.utility');


/**
 * User Model tests
 */

describe('User Model unit tests', function() {

    var user;

    beforeEach(function (done) {
        user = new User(constants.unitTests.TEST_USER_OBJECT);
        done();
    });

    describe('Method Save', function() {
        it('should be able to save without problems', function (done) {
            return user.save(function (err) {
                should.not.exist(err);
                done();
            });
        });

        it('should be able to show an error when try to save without firstName', function(done) {
            user.firstName = null;

            return user.save(function(err) {
                should.exist(err);
                done();
            });
        });

        it('should be able to show an error when try to save without email', function(done) {
            user.email = null;

            return user.save(function(err) {
                should.exist(err);
                done();
            });
        });

        it('should be able to show an error when try to save without password', function(done) {
            user.password = null;

            return user.save(function(err) {
                should.exist(err);
                done();
            });
        });

        it('should be able to show an error when an invalid firstName is provided', function(done) {
            user.firstName = '1e4265@';

            return user.save(function(err) {
                should.exist(err);
                done();
            });
        });

        it('should be able to show an error when an invalid lastName is provided', function(done) {
            user.lastName = '1e4265@';

            return user.save(function(err) {
                should.exist(err);
                done();
            });
        });

        it('should be able to show an error when an invalid email is provided', function(done) {
            user.email = '1e4265@';

            return user.save(function(err) {
                should.exist(err);
                done();
            });
        });

        it('should be able to show an error when an invalid value is for isTempPassword field', function(done) {
            user.isTempPassword = '2@';

            return user.save(function(err) {
                should.exist(err);
                done();
            });
        });
    });

    after(function (done) {
        User.remove().exec(function(err){
            done(err);
        });
    });
});
