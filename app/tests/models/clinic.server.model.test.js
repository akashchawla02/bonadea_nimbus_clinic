'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Clinic = mongoose.model('Clinic');

/**
 * Globals
 */
var clinic;

/**
 * Unit tests
 */
describe('Clinic Model Unit Tests:', function() {
	beforeEach(function(done) {
		clinic = new Clinic({
			// Add model fields
			// ...
		});

		done();
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return clinic.save(function(err) {
				should.not.exist(err);
				done();
			});
		});
	});

	afterEach(function(done) { 
		Clinic.remove().exec();
		User.remove().exec();
		
		done();
	});
});