'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    constants = require('../utility/constants.utility'),
    userModel = require('../models/user.server.model').userModel,
    clinicModel = require('../models/clinic.server.model').clinicModel,
    Schema = mongoose.Schema;

/**
 * Doctor Schema
 */
var DoctorSchema = new Schema({
    clinicId : {
        type: Schema.Types.ObjectId,
        ref: 'Clinic',
        required: 'Please provide the Clinic ID',
        index: true
    },
    userId : {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: 'Please provide the User id',
        index: true
    },
    qualifications : {
        type : [String]
    },
    practiceLocations : {
        type : [String]
    },
    appointments : {
        type : [{
            patientName : {
                type : String
            },
            patientContact : {
                type: String
            },
            scheduleOn : {
                type : Date
            }
        }]
    }
});

// Export the Mongoose model
var Doctor = mongoose.model('Doctor', DoctorSchema);

module.exports.createDefaultClinic = function(){
    clinicModel.find({}).exec(function (err, collection) {
        if (collection.length === 0) {

            var newClinic = new clinicModel({
                'name': 'Nimbus Clinic',
                'address' : {
                    'address1' : 'A-15, RSG Complex',
                    'address2' : 'Sector 52',
                    'city' : 'Noida',
                    'state' : 'Uttar Pradesh',
                    'pinCode' : '201301',
                    'contact' : '+91-9711-073-646'
                }
            });
            newClinic.save(function(errClinic,savedClinic){
                if(!errClinic){

                    var doctorUser = new userModel({
                        'firstName': 'Test',
                        'lastName': 'Doctor',
                        'email': 'doctor@nimbusclinic.com',
                        'password': 'doctor123',
                        'role': 'doctor'
                    });

                    doctorUser.save(function(errUser,savedUser){

                        if(!errUser){
                            var doctorDetail = new Doctor({
                                clinicId : savedClinic._id,
                                userId : savedUser._id,
                                qualifications : ['MBBS','MD Medicine'],
                                practiceLocations : ['Delhi','Gurgaon']
                            });

                            doctorDetail.save(function(errDoc,savedDoctor){

                            });
                        }
                    });
                }
            });
        }
    });
};



module.exports.doctorModel = Doctor;


