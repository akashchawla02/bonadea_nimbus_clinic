'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    constants = require('../utility/constants.utility'),
    Schema = mongoose.Schema;


var EmailTemplateSchema = new Schema({
    code: {
        type: String,
        trim: true,
        unique: true,
        required: constants.messages.TEMPLATE_CODE_REQUIRED
    },
    subject: {
        type: String,
        trim: true,
        required: constants.messages.EMAIL_SUBJECT_REQUIRED
    },
    templateId: { // Send Grid's template ID
        type: String,
        trim: true,
        index: true,
        required: constants.messages.SENDGRID_TEMPLATE_REQUIRED
    },
    fromName : {
        type: String,
        trim: true
    },
    fromEmailAddr : {
        type: String,
        trim: true,
        required: constants.messages.EMAIL_REQUIRED,
        match: [constants.regex.EMAIL_FIELD, constants.messages.INVALID_EMAIL_VALUE]
    },
    replyToEmailAddr : {
        type: String,
        trim: true,
        required: constants.messages.EMAIL_REQUIRED,
        match: [constants.regex.EMAIL_FIELD, constants.messages.INVALID_EMAIL_VALUE]
    }
});

// Export the Mongoose model
var EmailTemplate = module.exports = mongoose.model('EmailTemplate', EmailTemplateSchema);