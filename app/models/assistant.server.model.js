'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    constants = require('../utility/constants.utility'),
    Schema = mongoose.Schema;

/**
 * Doctor Schema
 */
var AssistantSchema = new Schema({
    userId : {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: 'Please provide the User id',
        index: true
    },
    doctorId :{
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: 'Please provide the User id',
        index: true
    },
    clinicId : {
        type: Schema.Types.ObjectId,
        ref: 'Clinic',
        required: 'Please provide the Clinic ID',
        index: true
    },
    permissions : {
        editCalendar :{
            type : Boolean,
            default: false
        },
        editPracticeLoc :{
            type : Boolean,
            default: false
        }
    }
});

// Export the Mongoose model
var Assistant = mongoose.model('Assistant', AssistantSchema);

module.exports.assistantModel = Assistant;


