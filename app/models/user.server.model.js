'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    bcrypt = require('bcrypt-nodejs'),
    constants = require('../utility/constants.utility'),
    Schema = mongoose.Schema;

/**
 * A Validation function for local strategy properties
 */
var validateNameFields = function(nameField) {
    return (!nameField || (nameField && nameField.match(constants.regex.NAME_FIELD)));
};

/**
 * User Schema
 */
var UserSchema = new Schema({
    firstName: {
        type: String,
        trim: true,
        required : constants.messages.FIRST_NAME_REQUIRED
    },
    lastName: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        unique: true,
        trim: true,
        index: true,
        required: constants.messages.EMAIL_REQUIRED,
        match: [constants.regex.EMAIL_FIELD, constants.messages.INVALID_EMAIL_VALUE]
    },
    role  :{
        type: String,
        index: true,
        required: constants.messages.EMAIL_REQUIRED,
        enum : constants.enums.USER_ROLES
    },
    password: {
        type: String,
        trim: true,
        required : constants.messages.PASSWORD_REQUIRED
    },
    isTempPassword:{
        type : Boolean,
        default : false
    },
    accessToken : {
        type: String,
        trim: true,
        default : ''
    }
});

// Execute before each user.save() call
UserSchema.pre('save', function(callback) {
    var user = this;

    if(user.isModified('password')){
        // Password changed so we need to hash it
        bcrypt.genSalt(5, function(err, salt) {
            if (err) return callback(err);

            bcrypt.hash(user.password, salt, null, function(err, hash) {
                if (err) return callback(err);
                user.password = hash;
                callback();
            });
        });
    }
});

UserSchema.methods.verifyPassword = function(password, callback) {
    bcrypt.compare(password, this.password, function(err, isMatch) {
        if (err) return callback(err);
        callback(null, isMatch);
    });
};


// Export the Mongoose model
var User = mongoose.model('User', UserSchema);

module.exports.userModel = User;

module.exports.createAdminUser = function(){
    User.find({}).exec(function (err, collection) {
        if (collection.length === 0) {
            User.create({
                'firstName': 'System',
                'lastName': 'Admin',
                'email': 'admin@bonadea.com',
                'password': 'admin@123',
                'role': 'admin'
            });
        }
    });
};


