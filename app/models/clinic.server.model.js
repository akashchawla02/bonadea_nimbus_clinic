'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    constants = require('../utility/constants.utility'),
    Schema = mongoose.Schema;

/**
 * A Validation function for local strategy properties
 */
var validateNameFields = function(nameField) {
    return (!nameField || (nameField && nameField.match(constants.regex.NAME_FIELD)));
};

/**
 * Clinic Schema
 */
var ClinicSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: constants.messages.FIRST_NAME_REQUIRED
    },
    address: {
        address1: {
            type: String,
            trim: true,
            required: true
        }, address2: {
            type: String,
            trim: true
        }, city: {
            type: String,
            trim: true,
            required: true
        },
        state: {
            type: String,
            trim: true,
            required: true
        },
        pinCode: {
            type: String,
            trim: true,
            required: true
        },
        contact: {
            type: String,
            trim: true,
            required: true
        }
    }
});

// Export the Mongoose model
var Clinic = mongoose.model('Clinic', ClinicSchema);

module.exports.clinicModel = Clinic;


