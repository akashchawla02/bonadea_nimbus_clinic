'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    domain = require('domain'),
    userCtrl = require('./user.server.controller'),
    errorHandler = require('./errors.server.controller'),
    Clinic = mongoose.model('Clinic'),
    Doctor = mongoose.model('Doctor'),
    EmailUtil = require('../utility/email-helper.utility.js').emailHelper,
    Assistant = mongoose.model('Assistant'),
    _ = require('lodash');

/**
 * Create a Clinic
 */
exports.addClinic = function(req, res) {
    var d = domain.create();

    d.on('error',function(error){
        var errorDetail = errorHandler.getErrorMessage(error);
        res.status(500).send(error);
    });

    var addClinic = new Clinic(req.body);

    addClinic.save(d.intercept(function(savedClinic){
        res.status(201).send();
    }));
};


/**
 * List of Clinics
 */
exports.getClinics = function(req, res) {

    var d = domain.create();

    d.on('error',function(error){
        var errorDetail = errorHandler.getErrorMessage(error);
        res.status(500).send(error);
    });

    Clinic.find().select({name : true,address:true})
        .exec(d.intercept(function(clinics){
            res.jsonp(clinics);
        }));
};


exports.getDoctors = function(req,res){

    var d = domain.create();

    d.on('error',function(error){
        var errorDetail = errorHandler.getErrorMessage(error);
        res.status(500).send(errorDetail);
    });
    var clinicId = req.params.clinicId;

    Doctor.find({'clinicId' : clinicId})
        .populate('userId',{firstName : true,lastName : true,email: true})
        .exec(d.intercept(function(doctors){
            res.jsonp(doctors);
        }));
};

exports.getAssistants = function(req,res){

    var d = domain.create();

    d.on('error',function(error){
        var errorDetail = errorHandler.getErrorMessage(error);
        res.status(500).send(errorDetail);
    });
    var clinicId = req.params.clinicId,
        doctorId = req.params.doctorId;

    Assistant.find({'clinicId' : clinicId,'doctorId' : doctorId})
        .populate('userId',{firstName : true,lastName : true,email: true})
        .exec(d.intercept(function(assistants){
            res.jsonp(assistants);
        }));
};

exports.addAssistants = function(req,res){

    var d = domain.create();

    d.on('error',function(error){
        var errorDetail = errorHandler.getErrorMessage(error);
        res.status(500).send(errorDetail);
    });
    var clinicId = req.params.clinicId,
        doctorId = req.params.doctorId;

    var assistants = req.body;
    var counter = 0;

    _.forEach(assistants,function(assistant){
        var userInfo ={
            firstName : assistant.firstName,
            lastName : assistant.lastName,
            email : assistant.email,
            role : 'assistant'
        };
        userCtrl.saveUserData(userInfo).then(function(result){
             var newAssistant = new Assistant({
                 clinicId : clinicId,
                 doctorId : doctorId,
                 userId : result.id,
                 permissions : assistant.permissions
             });

            newAssistant.save(d.intercept(function(savedDetails){
                counter ++;
                EmailUtil.sendWelcomeUserEmail(result).then(function(){
                    if(counter === assistants.length){
                        res.status(201).send();
                    }
                });

            }));
        }).catch(function(err){
            var errorDetail = errorHandler.getErrorMessage(err);
            res.status(500).send(errorDetail);
        });
    });
};