/**
 * Created by harendra.bisht on 09/06/15.
 */
'use strict';
process.env.TMPDIR  = global.__base + 'app/tmpfiles';
var multiparty      = require('multiparty' ),
    util            = require('util'),
    formidable      = require('formidable'),
    azure           = require('azure-storage'),
    deferred        = require('deferred'),
    CommonUtil      = require(global.__base +'app/utility/common-util.js' ).commonUtil,
    EmailUtil       = require('../utility/email-helper.utility.js').emailHelper,
    userCtrl        = require('../controllers/user.server.controller');

exports.create = function (req, res) {
    var mediaType   = req.query.type,
        form        = new multiparty.Form();

    form.uploadDir  = process.env.TMPDIR;
    form.parse(req, function(err, fields, files) {
        if (err) {
            res.writeHead(400, {'content-type': 'text/plain'});
            res.end('invalid request: ' + err.message);
            return;
        }
        var tmpFileLocation = files.file[0].path,
            fileSize = files.file[0].size;
        CommonUtil.readCsv(tmpFileLocation, userCtrl.saveUserData ).then(function (data) {

            EmailUtil.sendWelcomeUserEmail(data).then(function(){
                res.jsonp(data);
            },function(err){
                res.status(500).send();
            });
        }, function (err) {
            res.status(400 ).send();
        });
    });


    /*form.on('part', function(part) {
     if (!part.filename) return;

     var size = part.byteCount;
     var name = part.filename;
     console.log(size);
     var container = 'blobContainerName';

     blobService.createBlockBlobFromStream('videos', 'testdata.mp4', part, size, function(error) {
     if (error) {
     // error handling
     }
     //res.send('File uploaded successfully');
     });
     });
     form.on('close', function () {
     res.send('File uploaded successfully');
     });
     form.parse(req);*/
};

