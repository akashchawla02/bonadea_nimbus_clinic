'use strict';

/*
 Module Dependencies
 */
var mongoose    = require('mongoose'),
    User        = mongoose.model('User'),
    Doctor      = mongoose.model('Doctor'),
    Assistant   = mongoose.model('Assistant'),
    Clinic      = mongoose.model('Clinic'),
    domain      = require('domain'),
    deferred    = require('deferred'),
    errorCtrl   = require('../controllers/errors.server.controller'),
    authCtrl    = require('../controllers/auth.server.controller'),
    constants   = require('../utility/constants.utility'),
    _           = require('lodash');


exports.addUser = addUser;
exports.getUsers = getUsers;
exports.createUserAccessToken = createUserAccessToken;
exports.signIn = signIn;
exports.signOut = signOut;
exports.saveUserData = saveUser;

/*
 * Adds a User
 */
function addUser(req, res) {
    var userData = req.body;
    saveUser(userData).then(function(userInfo){
        userInfo = _.omit(userInfo,['isTempPassword','password']);
        res.json(userInfo);
    }).catch(function(error){
        res.status(500).send(error);
    });
}

/*
 * Gets the list of users
 */
function getUsers(req,res){

    var d = domain.create();
    d.on('error',function(err){
        res.status(500).send(err);
    });

    var userSelectList = {
        firstName : true,
        lastName : true,
        email : true,
        role:true
    };

    var userQuery = User.find();

    if(req.query.role){
        userQuery = userQuery.where({'role' : req.query.role});
    }

    userQuery.select(userSelectList).exec(d.intercept(function(users){
        res.send(users);
    }));
}

/*
 * Creates a JWT Access Token for User SignIn
 */
function createUserAccessToken(user){

    var def  = deferred();

    var d = domain.create();
    d.on('error',function(err){
        def.reject(err);
    });

    var userDetails = _.pick(user._doc,['email','firstName','lastName','role','_id']);

    fetchUserSpecificDetails(user._id,user.role).then(function(result){
        if(result){
            userDetails.extraInfo = result;
        }
        sendToken();
    }).catch(function(err){
        console.log(err);
        sendToken();
    });

    function sendToken(){
        var jwtTokenPayload = {
            user : userDetails
        };

        var accessToken = authCtrl.createJwtAccessToken(jwtTokenPayload);

        var updateExpr = {
            $set: {'accessToken':accessToken}
        };

        User.findByIdAndUpdate(user._id,updateExpr)
            .exec(d.intercept(function(savedUser){
                def.resolve(savedUser);
            }));
    }

    return def.promise;
}



/*
 * Authenticates a User and create access token for SignIn
 */
function signIn(req,res){

    var param = req.body;
    var d = domain.create();
    d.on('error',function(err){
        return res.status(500).send(err);
    });

    User.findOne({ email: param.email }, d.intercept(function (user) {
        // No user found with that username
        if (!user) { return res.status(401).send(constants.messages.EMAIL_OR_PASSWORD_INCORRECT); }

        // Make sure the password is correct
        user.verifyPassword(param.password, d.intercept(function(isMatch) {
            // Password did not match
            if (!isMatch) { return res.status(401).send(constants.messages.EMAIL_OR_PASSWORD_INCORRECT);  }

            // Success
            createUserAccessToken(user).then(function(userWithAccessToken){
                return res.status(200).send(userWithAccessToken.accessToken);

            }).catch(function(err){
                return res.status(500).send(errorCtrl.getErrorMessage(err));
            });
        }));
    }));
}

/*
 * Sign Outs a User and clears the access token
 */
function signOut(req,res){
    var user = req.user;

    var d = domain.create();
    d.on('error',function(err){
        res.status(500).send(err);
    });

    var updateExpr = {
        $set: {'accessToken': ''}
    };

    User.findByIdAndUpdate(user._id,updateExpr)
        .exec(d.intercept(function(savedUser){
            res.status(204).send();
        }));
}

/*
 * Saves a user's details
 */
function saveUser(data) {
    if(!data.password){
        data.password = Math.random().toString(36).slice(-8);
        data.isTempPassword = true;
    }
    if(!data.role){
        data.role = 'doctor';
    }
    var user    = new User(data),
        defer   = deferred();

    user.save(function (err, savedUser) {
        if(err){
            defer.reject(err);
        } else {
            data.id = savedUser._id;
            defer.resolve(data);
        }
    });

    return defer.promise;
}

function fetchUserSpecificDetails(userId,role){
    var defer   = deferred();

    switch(role){
        case 'doctor':
            Doctor.findOne({'userId' : userId})
                .populate('clinicId')
                .select({clinicId : true, qualifications: true,appointments:true,practiceLocations:true})
                .exec(function(err,doctor){
                    if(!err){
                        defer.resolve(doctor);
                    }else{
                        defer.reject(err);
                    }
                });
            break;
        case 'assistant' :
            Assistant.findOne({'userId' : userId})
                .populate('clinicId')
                .populate('doctorId')
                .select({clinicId : true, doctorId: true,permissions:true})
                .exec(function(err,doctor){
                    if(!err){
                        defer.resolve(doctor);
                    }else{
                        defer.reject(err);
                    }
                });
                break;
        default :
            defer.resolve();

    }

    return defer.promise;
}
