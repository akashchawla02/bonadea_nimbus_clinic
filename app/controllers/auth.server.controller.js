'use strict';

/*
 Module Dependencies
 */
var passport = require('passport'),
    jwt      = require('jwt-simple'),
    config   = require('../../config/config');

exports.createJwtAccessToken = function(jwtTokenPayload){

    var currentDate = new Date();

    /*//Getting current time in Unix timestamp
    var currentUnixTime = Math.round(currentDate.getTime() / 1000);

    // “Issued at” time, in Unix time, at which the token was issued
    jwtTokenPayload.iat = currentUnixTime;

    // “Not before” time that identifies the time before which the JWT must not be accepted for processing
    jwtTokenPayload.nbf = currentUnixTime;
    */

    // Token expiration time defined in Unix time
    currentDate.setMinutes(currentDate.getMinutes() + config.accessToken.expirationInMinutes);
    jwtTokenPayload.exp  = Math.round(currentDate.getTime() / 1000);

    //Encoding the token payload with the secret key
    return jwt.encode(jwtTokenPayload, config.accessToken.secretKey);
};

/*
 Verifies whether the access token has been expired or not
 */
exports.verifyTokenExpiration = function(accessToken){
    var decodedPayload = jwt.decode(accessToken, config.accessToken.secretKey),
        currentDate = new Date();
    var currentUnixTime = Math.round(currentDate.getTime() / 1000);

    return currentUnixTime < decodedPayload.exp;
};

/*
 Authenticate sign in API with basic authentication strategy
 */
exports.signInAuthentication = passport.authenticate('basic', { session : false });

/*
 Authenticate sign in API with custom access-token authentication strategy
 */
exports.tokenAuthentication = passport.authenticate('token', { session : false });

