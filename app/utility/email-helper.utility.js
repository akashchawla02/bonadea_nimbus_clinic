'use strict';

/*
 * Module Dependencies
 */
var deferred        = require('deferred'),
    domain          = require('domain'),
    config          = require('../../config/config'),
    sendGrid        = require('sendgrid')(config.sendGrid.userName, config.sendGrid.password),
    mongoose        = require('mongoose'),
    EmailTemplate   = mongoose.model('EmailTemplate'),
    _               = require('lodash');

var EmailHelper = function (){};

/*
    Email Template Seed Data
 */
var templates = {
    WELCOME_USER: {
        code: 'WELCOME_USER',
        subject: 'Welcome To Bonadea mHealth!',
        templateId: '19898c39-bc1c-4306-87ad-cecdf14718c3',
        fromName : 'Bondea mHealth',
        fromEmailAddr: 'accounts@bonadeamhealth.com',
        replyToEmailAddr: 'noreply@bonadeamhealth.com'
    }
};

/*
 Inserts seed data for Email Templates
 */
EmailHelper.prototype.seedEmailTemplates = function() {
    EmailTemplate.find({}).exec(function (err, collection) {
        if (collection.length === 0) {
            EmailTemplate.create(templates.WELCOME_USER);
        }
    });
};

/*
 Sends New User's Welcome Email
 */
EmailHelper.prototype.sendWelcomeUserEmail = function(emailList){
    var defer = deferred(),
        d = domain.create();

    d.on('error',function(err){
        defer.reject(err);
    });

    if (emailList.length > 0) {
        //Get the email template details from database
        EmailTemplate.findOne({code: templates.WELCOME_USER.code}).exec(d.intercept(function (template) {
            var recipients = _.pluck(_.sortBy(emailList, 'email'), 'email'),
                passwords = _.pluck(_.sortBy(emailList, 'email'), 'password'),
                firstNames = _.pluck(_.sortBy(emailList, 'email'), 'firstName');

            var email = new sendGrid.Email();
            email.subject = template.subject;
            email.from = template.fromEmailAddr;
            email.fromname = template.fromName;
            email.replyto = template.replyToEmailAddr;
            email.text = email.html = ' ';

            email.setTos(recipients);
            email.addSubstitution(':firstName:', firstNames);
            email.addSubstitution(':password:', passwords);

            // add filter settings for template
            email.addFilter('templates', 'enable', 1);
            email.addFilter('templates', 'template_id', template.templateId);

            sendGrid.send(email, function (err, json) {
                if (err) {
                    defer.reject(err);
                } else {
                    defer.resolve(true);
                }
            });

        }));
    }else{
        defer.resolve(true);
    }

    return defer.promise;
};

exports.emailHelper = new EmailHelper();
