'use strict';

var emailHelper = require('../utility/email-helper.utility.js').emailHelper,
    userHelper = require('../models/user.server.model'),
    doctorHelper = require('../models/doctor.server.model');

var SeedDataHelper = function () {};

SeedDataHelper.prototype.createSeedData = function () {
    userHelper.createAdminUser();
    emailHelper.seedEmailTemplates();
    doctorHelper.createDefaultClinic();
};


exports.seedDataHelper = new SeedDataHelper();