'use strict';

/*
 This file contains all the constants used.
 */
(function(constants){

    constants.regex = {
        OBJECT_ID : /^[0-9a-fA-F]{24}$/,
        EMAIL_FIELD : /.+\@.+\..+/,
        NAME_FIELD : /^[a-zA-Z]{2,30}$/
    };

    constants.messages = {
        EMAIL_REQUIRED : 'Please provide the email address.',
        INVALID_EMAIL_VALUE : 'Please provide a valid email address.',
        FIRST_NAME_REQUIRED : 'Please provide the first name.',
        INVALID_NAME_VALUE : 'Please provide a valid name with alphabets only upto 30 characters.',
        PASSWORD_REQUIRED : 'Please provide the password.',
        EMAIL_OR_PASSWORD_INCORRECT : 'Email or Password is incorrect.',
        TEMPLATE_CODE_REQUIRED : 'Please provide the email template code.',
        EMAIL_SUBJECT_REQUIRED : 'Please provide the subject for the email.',
        SENDGRID_TEMPLATE_REQUIRED : 'Please provide SendGrid Template ID',
        USER_ROLE_REQUIRED : 'Please provide the user role.'
    };

    constants.enums = {
        USER_ROLES : ['admin','doctor','assistant']
    };

    constants.unitTests = {
        TEST_USER_OBJECT: {
            firstName: 'Test',
            lastName: 'User',
            email: 'testuser@test.com',
            password: 'test123',
            isTempPassword : false,
            role : 'admin'
        }
    };

}(module.exports));