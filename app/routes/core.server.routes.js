'use strict';

module.exports = function(app) {
	// Root routing
	var core = require('../../app/controllers/core.server.controller');
	var fileUpload = require('../../app/controllers/file-upload.server.controller');

	app.route('/').get(core.index);

    app.route('/file-upload')
        .post(fileUpload.create);
    app.route('/users')
        .get(core.getTeacher);
};