'use strict';

/**
 * Module dependencies.
 */
var userCtrl = require('../controllers/user.server.controller.js'),
    authCtrl = require('../../app/controllers/auth.server.controller');

module.exports = function(app) {

    // Create endpoint handlers for /users
    app.route('/api/v1/users')
        .post(userCtrl.addUser)
        .get(authCtrl.tokenAuthentication,userCtrl.getUsers);

    app.route('/api/v1/users/signin')
        .post(authCtrl.signInAuthentication,userCtrl.signIn);

    app.route('/api/v1/users/signout')
        .get(authCtrl.tokenAuthentication,userCtrl.signOut);
};

