'use strict';

var clinicCtrl = require('../controllers/clinic.server.controller'),
    authCtrl = require('../../app/controllers/auth.server.controller');

module.exports = function(app) {
    // Create endpoint handlers for /clinics
    app.route('/api/v1/clinics')
        .post(authCtrl.tokenAuthentication,clinicCtrl.addClinic)
        .get(authCtrl.tokenAuthentication,clinicCtrl.getClinics);

    app.route('/api/v1/clinics/:clinicId/doctors')
        .get(authCtrl.tokenAuthentication,clinicCtrl.getDoctors);

    app.route('/api/v1/clinics/:clinicId/doctors/:doctorId/assistants')
        .get(authCtrl.tokenAuthentication,clinicCtrl.getAssistants)
        .post(authCtrl.tokenAuthentication,clinicCtrl.addAssistants);
};