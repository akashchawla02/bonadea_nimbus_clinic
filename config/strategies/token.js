'use strict';

/**
 * Module dependencies.
 */
var passport      = require('passport'),
    TokenStrategy = require('passport-accesstoken').Strategy,
    authCtrl      = require('../../app/controllers/auth.server.controller'),
    User          = require('mongoose').model('User');

module.exports = function() {
    // Using Token strategy
    var strategyOptions = {
        tokenHeader:    'x-access-token'
    };

    passport.use(new TokenStrategy(strategyOptions,
        function(token, callback) {
            User.findOne({ accessToken: token }, function (err, user) {
                if (err) { return callback(err); }

                // No user found with that accessToken
                if (!user) { return callback(null, false); }

                // Make sure the password is correct
                if(!authCtrl.verifyTokenExpiration(token)) {
                    // Password did not match
                    return callback(null, false);
                }

                // Success
                return callback(null, user);
            });
        }
    ));
};
