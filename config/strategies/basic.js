'use strict';

/**
 * Module dependencies.
 */
var passport        = require('passport'),
    BasicStrategy   = require('passport-http').BasicStrategy,
    config          = require('../../config/config');

module.exports = function() {
    // Using Basic strategy

    passport.use(new BasicStrategy(
        function(username, password, callback) {
            var credentials = config.signInCredentials,
                isValidUser = (username === credentials.webAppUserName && password === credentials.webAppPassWord) ||
                    (username === credentials.winAppUserName && password === credentials.winAppUserName);

            return callback(null, isValidUser);
        }
    ));
};
