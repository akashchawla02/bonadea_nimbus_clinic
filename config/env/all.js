'use strict';

module.exports = {
	app: {
		title: 'Bonadea Clinic',
		description: 'Bonadea Clinic Management System',
		keywords: 'Bonadea, Clinic'
	},
	port: process.env.PORT || 4000,
	secure: process.env.SECURE || false,
	templateEngine: 'swig',
	sessionSecret: 't6RyR5@xmj3r*sa',
	sessionCollection: 'sessions',
	assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.css',
				'public/lib/bootstrap/dist/css/bootstrap-theme.css',
			],
			js: [
				'public/lib/angular/angular.js',
                'public/lib/jquery/dist/jquery.js',
                'public/lib/bootstrap/dist/js/bootstrap.js',
				'public/lib/angular-resource/angular-resource.js',
				'public/lib/angular-animate/angular-animate.js',
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
				'public/lib/ng-file-upload/ng-file-upload-shim.js',
				'public/lib/ng-file-upload/ng-file-upload.js',
				'public/lib/angular-local-storage/dist/angular-local-storage.js'
			]
		},
		css: [
			'public/modules/**/css/*.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js'
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};
