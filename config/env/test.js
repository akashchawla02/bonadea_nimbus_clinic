'use strict';

module.exports = {
    db: 'mongodb://localhost/bonadea-clinic-test',
    port: 3001,
    app: {
        title: 'GS-Lite - Test Environment',
        env : 'test'
    },
    mailer: {
        from: process.env.MAILER_FROM || 'MAILER_FROM',
        options: {
            service: process.env.MAILER_SERVICE_PROVIDER || 'MAILER_SERVICE_PROVIDER',
            auth: {
                user: process.env.MAILER_EMAIL_ID || 'MAILER_EMAIL_ID',
                pass: process.env.MAILER_PASSWORD || 'MAILER_PASSWORD'
            }
        }
    },
    signInCredentials :{
        webAppUserName : 'bonadea-Web',
        webAppPassWord : 'bonadeaWebXa123'
    },
    accessToken : {
        secretKey : '1iBaRftCorkYYFEfMs93dYeAccessTokenJn58n',
        expirationInMinutes : 30
    },
    sendGrid : {
        userName : 'akash0208',
        password : 'akash0208',
        smtpServer : 'smtp.sendgrid.net'
    }
};