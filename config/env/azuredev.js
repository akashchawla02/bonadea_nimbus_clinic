'use strict';

module.exports = {
    db: process.env.MONGOLAB_URI || 'mongodb://localhost/bonadea-clinic',
    app: {
        title: 'GS-Lite - Development Environment'
    },
    mailer: {
        from: process.env.MAILER_FROM || 'MAILER_FROM',
        options: {
            service: process.env.MAILER_SERVICE_PROVIDER || 'MAILER_SERVICE_PROVIDER',
            auth: {
                user: process.env.MAILER_EMAIL_ID || 'MAILER_EMAIL_ID',
                pass: process.env.MAILER_PASSWORD || 'MAILER_PASSWORD'
            }
        }
    },
    signInCredentials :{
        webAppUserName : 'bonadea-Web',
        webAppPassWord : 'bonadeaWebXa123'
    },
    accessToken : {
        secretKey : '1iBaRftCorkYYFEfMs93dYeAccessTokenJn58n',
        expirationInMinutes : 30
    },
    sendGrid : {
        userName : 'bonadeaclinic',
        password : 'akash0208',
        smtpServer : 'smtp.sendgrid.net'
    }
};