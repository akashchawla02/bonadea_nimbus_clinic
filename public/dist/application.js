'use strict';
// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function () {
	// Init module configuration options
	var applicationModuleName = 'GSLite',
	    applicationModuleVendorDependencies = ['ngResource',  'ngAnimate',  'ui.router', 'ngFileUpload', 'ui.bootstrap'],

	   // Add a new vertical module
	    registerModule = function (moduleName, dependencies) {
            // Create angular module
            angular.module(moduleName, dependencies || []);

            // Add the module to the AngularJS configuration file
            angular.module(applicationModuleName).requires.push(moduleName);
	    };

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();

'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider', '$httpProvider',
	function($locationProvider, $httpProvider) {
		$locationProvider.hashPrefix('!');

        $httpProvider.interceptors.push(["$timeout", "$injector", function($timeout, $injector){
            var $http, $state;
            $timeout(function(){
                $http = $injector.get('$http');
                $state = $injector.get('$state');
            }, 200);
            return {
                request: function(config) {
                    return config;
                },
                requestError: function (rejection) {
                    return rejection;
                }

            };
        }]);
	}
]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});
//--> Check if user is logged in
/*angular.module(ApplicationConfiguration.applicationModuleName).run(['$rootScope', '$location', 'Authentication', function($rootScope, $location, Authentication){
    //----> check the state change of route
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
        //---> Check user authentication
        var currentUser = Authentication.user ? true : false,
            isAuthenticateUrl = toState.data ? toState.data.requestAuthenticate : false;

        if(isAuthenticateUrl && !currentUser){
            //---> Move to login if not authenticated
            $rootScope.$evalAsync(function () {
                $location.path('/login');
            });
            event.preventDefault();
        }
    });

}]);*/

'use strict';

// Use application configuration module to register a new module
ApplicationConfiguration.registerModule('admin');

'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core', [ 'ui.bootstrap']);
'use strict';

//Setting up route
angular.module('admin').config(['$stateProvider',
	function($stateProvider) {
		// Admin state routing
		$stateProvider.
		state('clinic-management', {
			url: '/clinic-management',
			templateUrl: 'modules/admin/views/clinic-management.client.view.html'
		});
	}
]);
/**
 * Created by harendra.bisht on 19/06/15.
 */
'use strict';
angular.module('admin')
    .controller('FileUploadController', FileUploadController);

/*@ngInject*/

function FileUploadController ($scope, $modalInstance, items) {
    /*jshint validthis:true*/
    var vm = this;
    vm.close = closeFileUpload;

    function closeFileUpload () {
        $modalInstance.close();
    }

}
FileUploadController.$inject = ["$scope", "$modalInstance", "items"];
'use strict';
angular.module('admin')
    .controller('ImportTeacherController', ImportTeacherController);
/*@ngInject*/

function ImportTeacherController ($scope, Upload, $modalInstance, items) {
    /*jshint validthis: true*/
    var vm = this;

    vm.close = modalClose;

    vm.upload = function (files) {
        if (files && files.length) {
            angular.forEach(files, function (file){

                Upload.upload({
                    url: '/file-upload',
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.uploadProgress = progressPercentage;

                }).success(function (data, status, headers, config) {
                    console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                });
            });
        }
    };

    function modalClose () {
        $modalInstance.close();
    }
}
ImportTeacherController.$inject = ["$scope", "Upload", "$modalInstance", "items"];
'use strict';

angular.module('admin')
    .controller('TeacherManagementController', TeacherManagementController);

/*@ngInject*/

function TeacherManagementController ($modal, User) {
    var vm = this,
        items;

    vm.init = function () {
        vm.getTeachers();
    };

    vm.importTeacher = function () {
        var importedResult = $modal.open({
            templateUrl:'modules/admin/views/import-user.client.view.html',
            controller:'ImportUserController',
            controllerAs: 'vm',
            resolve: {
                items: function () {
                    return items;
                }
            }
        });

        importedResult.result.then(function () {
            vm.getTeachers();
        }).catch(function () {
            vm.getTeachers();
        });
    };

    vm.getTeachers = function () {
        vm.teacherList =  User.query();
        vm.teacherList.$promise.then(function (result) {
            vm.teacherList = result;
        }, function (error){
            console.log(error);
        });
    };


}
TeacherManagementController.$inject = ["$modal", "User"];

'use strict';

angular.module('admin')
    .controller('VideoManagementController', VideoManagementController);
/*@ngInject*/

function VideoManagementController ($scope, $modal) {
    /*jshint validthis: true*/
    var vm = this;
    vm.uploadVideo = uploadVideo;

    function uploadVideo () {
        var uploadModal = $modal.open({
            templateUrl: '/modules/admin/views/file-upload.client.view.html',
            controller: 'FileUploadController',
            controllerAs: 'vm',
            resolve: {
                items: function () {
                    return ;
                }
            }
        });

        uploadModal.result.then(function (files) {

        } );
    }
}
VideoManagementController.$inject = ["$scope", "$modal"];
'use strict';
angular.module('admin')
    .factory('User', ['$resource', function ($resource) {
        return $resource('/users/:id', {id:'@_id'}, {
           update: {
               method: 'PUT'
           }
        });
    }]);
'use strict';

//Setting up route
angular.module('core')
    .config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
	}
]);
'use strict';

angular.module('core')
    .controller('HeaderController', HeaderController);
/*@ngInject*/
function HeaderController ($scope, $state){
    /*jshint validthis:true*/
    var vm = this,
    currentPath = $state.current.name;

    $scope.$on('$stateChangeSuccess', function (ev, data){
        vm.activePath = data.name;
    });
}
HeaderController.$inject = ["$scope", "$state"];
'use strict';

angular.module('core')
    .service('HttpService', ['$http', '$q', '$log', '$rootScope',
    function ($http, $q, $log, $rootScope) {
        var self = this,
            defaults = {
                params: '',
                data: {},
                transformRequest: null,
                cache: true
            },

            errorStatus = {
                0: 'Request timeout. Please try again.',
                401: 'Unauthorized request. You are not authorized for this request.',
                500: 'Server Error. There is some server error. Please try later.'
            };

        function handleError(response) {
            if (response.status === 0 || response.status === 401 || response.status === 500) {
                $rootScope.connectionErr = true;
                $rootScope.responseError  = errorStatus[response.status];
            }

            if (!angular.isObject(response.data)) {
                return ($q.reject(response));
            }


            // Otherwise, use expected error message.
            return ($q.reject(response));
        }


        // transform the successful response, unwrapping the application data
        // from the API response payload.
        function handleSuccess(response) {
//            $rootScope.gblLoader = false;
            $rootScope.connectionErr = response.status === 0 || response.status === 401;
            return (response);

        }

        // Public API
        /*
         ** Call for POST request
         */
        self.postData = function (url, config) {
            var configure = angular.extend({}, defaults, config);
            //configure.headers['validation-token'] = ipCookie('user')? ipCookie('user').token : '';
            var response = $http({
                method: 'POST',
                url: url,
                data: configure.data,
                headers: configure.headers,
                transformRequest: configure.transformRequest
            });
            return (response.then(handleSuccess, handleError));
        };
        /*
         ** Call for get Request from server
         */
        self.getData = function (url, config) {
            var configure = angular.extend({}, defaults, config);
            //configure.headers['validation-token'] = ipCookie('user')? ipCookie('user').token : '';
            var response = $http.get(url, {
                params: configure.params,
                timeout: 6000,
                headers: configure.headers
            });

            return response.then(handleSuccess, handleError);
        };

        self.putData = function (url, config) {
            var configure = angular.extend({}, defaults, config);
            //configure.headers['validation-token'] = ipCookie('user')? ipCookie('user').token : '';
            var response = $http({
                method: 'PUT',
                url: url,
                data: configure.data,
                headers: configure.headers,
                transformRequest: configure.transformRequest
            });
            return (response.then(handleSuccess, handleError));
        };
        self.delData = function (url, config) {
            var configure = angular.extend({}, defaults, config);
            //configure.headers['validation-token'] = ipCookie('user')? ipCookie('user').token : '';
            var response = $http({
                method: 'DELETE',
                url: url,
                headers: configure.headers
            });
            return (response.then(handleSuccess, handleError));
        };

    }
]);
