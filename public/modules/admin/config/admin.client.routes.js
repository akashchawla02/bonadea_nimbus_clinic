'use strict';

//Setting up route
angular.module('admin').config(['$stateProvider','localStorageServiceProvider', '$urlRouterProvider',
	function($stateProvider, localStorageServiceProvider, $urlRouterProvider) {
       // $urlRouterProvider.otherwise('/teacher-management');
		// Admin state routing
        localStorageServiceProvider.setStorageType('sessionStorage');
		$stateProvider.
		state('login', {
			url: '/login',
			templateUrl: 'modules/admin/views/login.client.view.html',
            data: {
                requestAuthenticate: false
            }
		}).
		state('schedule-management', {
			url: '/schedule-management',
			templateUrl: 'modules/admin/views/schedule-management.client.view.html',
            data: {
                requestAuthenticate: true
            }
		}).
		state('clinic-management', {
			url: '/clinic-management',
			templateUrl: 'modules/admin/views/clinic-management.client.view.html',
                data: {
                    requestAuthenticate: true
                }
		});
	}
]);