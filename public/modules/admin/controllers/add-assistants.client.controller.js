'use strict';
angular.module('admin')
    .controller('AddAssistantsController', AddAssistantsController);
/*@ngInject*/

function AddAssistantsController ($scope, HttpService, StorageService,$modalInstance, items,$location) {
    /*jshint validthis: true*/
    var vm = this;

    vm.close = modalClose;
    vm.clinic = {address : {}};

    vm.showFormCount = true;
    vm.showFormName = false;
    vm.showFormPermission = false;

    vm.saveAssistants = function () {

        var user = StorageService.user();
        var url = '/api/v1/clinics/'+ user.extraInfo.clinicId._id +'/doctors/'+ user._id +'/assistants';
        HttpService
            .postData(url, {
                headers: {
                    'x-access-token' : StorageService.getToken(),
                    'Content-Type' : 'application/json'
                },
                data :JSON.stringify(vm.assistants)
            })
            .then(function (data) {
                modalClose();
                $location.path('/clinic-management');
            }).catch(function (error){
                console.log(error);
                modalClose();
                $location.path('/login');
            });

    };

    vm.populateFields = function(formType){
        switch(formType){
            case 'assistantCount' :
                vm.showFormCount = false;
                vm.showFormName = true;
                vm.showFormPermission = false;
                vm.assistants = [];
                for(var i=0;i< vm.totalAssistant;i++){
                    var assistantObj = {
                        permissions : {
                            editCalendar : false,
                            editPracticeLoc : false
                        }
                    };
                    vm.assistants.push(assistantObj);
                }
                break;
            case 'assistantName':
                checkAllAssistantNames();
                if(vm.errorMessages && vm.errorMessages.length ===0){
                    vm.isError = false;
                    vm.showFormCount = false;
                    vm.showFormName = false;
                    vm.showFormPermission = true;
                }else{
                    vm.isError = true;
                }
                break;

        }
    };

    function checkAllAssistantNames(){
        vm.errorMessages = [];
        vm.assistants.forEach(function(assistant,index){
         if(!(assistant.firstName && assistant.email && assistant.email.match(/.+\@.+\..+/))){
             var assistantNum = vm.assistants.indexOf(assistant) + 1;
             vm.errorMessages.push('Enter valid FirstName and Email for Assistant#' + assistantNum);
         }
      });
    }

    vm.getTabName = function(assistant){
        var tabName = assistant.firstName;
        if(assistant.lastName && assistant.lastName.trim().length > 0){
            tabName = tabName + ' ' + assistant.lastName.substr(0,2) +'.';
        }
        return tabName;
    };

    function modalClose () {
        $modalInstance.close();
    }
}
