'use strict';

angular.module('admin')
    .controller('ClinicManagementController', ClinicManagementController);

/*@ngInject*/

function ClinicManagementController ($scope, $modal, User,StorageService,HttpService) {
    /*jshint validthis: true*/
    var vm = this,
        items;

    vm.init = function () {
        vm.user= StorageService.user();
        vm.getClinics();
        vm.getUsers();
    };

    vm.importUsers = function () {
        var importedResult = $modal.open({
            templateUrl:'modules/admin/views/import-user.client.view.html',
            controller:'ImportUserController',
            controllerAs: 'vm',
            resolve: {
                items: function () {
                    return items;
                }
            }
        });

        importedResult.result.then(function () {
            vm.getUsers();
        }).catch(function () {
            vm.getUsers();
        });
    };

    vm.addClinic = function () {
        var importedResult = $modal.open({
            templateUrl:'modules/admin/views/add-clinic.client.view.html',
            controller:'AddClinicController',
            controllerAs: 'vm',
            resolve: {
                items: function () {
                    return items;
                }
            }
        });

        importedResult.result.then(function () {
            vm.getUsers();
        }).catch(function () {
            vm.getUsers();
        });
    };

    vm.addAssistants = function(){
        var importedResult = $modal.open({
            templateUrl:'modules/admin/views/add-assistants.client.view.html',
            controller:'AddAssistantsController',
            controllerAs: 'vm',
            resolve: {
                items: function () {
                    return items;
                }
            }
        });

        importedResult.result.then(function () {
            vm.getUsers();
        }).catch(function () {
            vm.getUsers();
        });
    };

    vm.getUsers = function (role) {
        var url = '/api/v1/users';
        if(role){
            url = url +'?role=' + role;
        }

        HttpService
            .getData(url, {
                headers: {
                    'x-access-token' : StorageService.getToken(),
                    'Content-Type' : 'application/json'
                }
            })
            .then(function (result) {
                var userList = vm.allUsers = result.data;
                switch(vm.user.role){
                    case 'admin':
                        userList = userList.filter(function(user){
                            return user.role === 'doctor';
                        });
                        break;
                    case 'doctor':
                        userList = userList.filter(function(user){
                            return user.role === 'assistant';
                        });
                        break;
                }
                vm.userList = userList;
            }).catch(function (error){
                console.log(error);
            });
    };

    vm.getClinics = function(){
        HttpService
            .getData('/api/v1/clinics', {
                headers: {
                    'x-access-token' : StorageService.getToken(),
                    'Content-Type' : 'application/json'
                }
            })
            .then(function (result) {
                vm.clinicList = result.data;
            }).catch(function (error){
                console.log(error);
            });
    };

}
