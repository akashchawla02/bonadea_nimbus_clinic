/**
 * Created by harendra.bisht on 19/06/15.
 */
'use strict';
angular.module('admin')
    .controller('FileUploadController', FileUploadController);

/*@ngInject*/

function FileUploadController ($scope, $modalInstance, items) {
    /*jshint validthis:true*/
    var vm = this;
    vm.close = closeFileUpload;

    function closeFileUpload () {
        $modalInstance.close();
    }

}