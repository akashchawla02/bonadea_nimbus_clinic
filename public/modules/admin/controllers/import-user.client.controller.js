'use strict';
angular.module('admin')
    .controller('ImportUserController', ImportUserController);
/*@ngInject*/

function ImportUserController ($scope, Upload, $modalInstance, items) {
    /*jshint validthis: true*/
    var vm = this;

    vm.close = modalClose;

    vm.acceptType = ['application/vnd.ms-excel','text/csv','text/tsv','application/octet-stream'];
    vm.upload = function (files) {
        if (files && files.length) {
            angular.forEach(files, function (file){

                Upload.upload({
                    url: '/file-upload',
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.uploadProgress = progressPercentage;

                }).success(function (data, status, headers, config) {
                    console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                });
            });
        }
    };

    function modalClose () {
        $modalInstance.close();
    }
}