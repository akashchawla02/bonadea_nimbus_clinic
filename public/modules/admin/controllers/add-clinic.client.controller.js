'use strict';
angular.module('admin')
    .controller('AddClinicController', AddClinicController);
/*@ngInject*/

function AddClinicController ($scope, HttpService, StorageService,$modalInstance, items,$location) {
    /*jshint validthis: true*/
    var vm = this;

    vm.close = modalClose;
    vm.clinic = {address : {}};

    vm.submitClinic = function (form) {
        if(form.$valid){
            HttpService
                .postData('/api/v1/clinics', {
                    headers: {
                        'x-access-token' : StorageService.getToken(),
                        'Content-Type' : 'application/json'
                    },
                    data :JSON.stringify(vm.clinic)
                })
                .then(function (data) {
                    modalClose();
                    $location.path('/clinic-management');
                }).catch(function (){
                    modalClose();
                    $location.path('/login');
                });
        }
    };

    function modalClose () {
        $modalInstance.close();
    }
}