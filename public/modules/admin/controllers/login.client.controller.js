'use strict';

angular
    .module('admin')
    .controller('LoginController', LoginController);
/*@ngInject*/

function LoginController ($scope, $location, HttpService, $window, StorageService, $rootScope) {
    /*jshint validthis:true*/
    var vm = this;
    vm.doLogin = function (user, isValid) {
        var loginData = user;

        if(!isValid){
            return;
        }
        vm.isLoading = true;
        HttpService
            .postData('/api/v1/users/signin', {
                headers: {
                    'Authorization' : 'Basic ' +btoa('bonadea-Web:bonadeaWebXa123'),
                    'Content-Type' : 'application/json'
                },
                data :JSON.stringify({email: loginData.email,password:loginData.password})
            })
            .then(function (data) {
                var jwtToken = data.data,
                    base64Url = jwtToken.split('.')[1],
                    base64 = base64Url.replace('-', '+').replace('_', '/');

                StorageService.setToken(jwtToken);
                $location.path('/clinic-management');
                vm.isLoading = false;
            }).catch(function (){
                $location.path('/login');
                vm.isLoading = false;
                vm.serverError = true;
            });
    };
}