'use strict';

(function() {
	// Teacher management Controller Spec
	describe('Teacher management Controller Tests', function() {
		// Initialize global variables
		var TeacherManagementController,
			scope,
			$httpBackend,
			$stateParams,
			$location,
            vm;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service


		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Teacher management controller.
			TeacherManagementController = $controller('TeacherManagementController', {
				$scope: scope
			});
            vm = TeacherManagementController;
		}));
        afterEach(function() {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });
		it('Should get teachers list', inject(function(User) {
			// The test logic

            var sampleUser = new User({
                firstName: 'An Article about MEAN',
                lastName: 'MEAN rocks!',
                email: 'MEAN rocks!'
            });

            // Create a sample articles array that includes the new article
            var sampleUsers = [sampleUser];

            // Set GET response
            $httpBackend.expectGET('users').respond(sampleUsers);

            // Run controller functionality
            vm.getUsers();
            $httpBackend.flush();
            // Test scope value
            expect(vm.teacherList).toEqualData(sampleUsers);

		}));
	});
}());