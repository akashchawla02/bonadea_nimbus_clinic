'use strict';

angular.module('core')
    .controller('HeaderController', HeaderController);
/*@ngInject*/
function HeaderController ($scope, $state, StorageService, HttpService){
    /*jshint validthis:true*/
    var vm = this,
    currentPath = $state.current.name;
    vm.active = true;
    $scope.$on('$stateChangeSuccess', function (ev, data){
        vm.activePath = data.name;
        if(data.name === 'login'){
            vm.active = false;
        }
    });

    vm.user = StorageService.user();

    vm.doLogout = function () {

        HttpService
            .getData('/api/v1/users/signout', {
                headers: {
                    'x-access-token' : StorageService.getToken()
                }
            })
            .then(function () {
                StorageService.removeToken();
            })
            .catch(function (error, status) {
                //Handle 401 case
                StorageService.removeToken();
            });
    };

}