'use strict';

angular.
    module('core')
.factory('StorageService', StorageService);
/*@ngInject*/
function StorageService (localStorageService, $window, $location){
    var storage ={};
    storage.user = function () {
        var token = localStorageService.get('token' );
        if(!token){
            return null;
        }
        var base64Url = token.split('.')[1 ],
            base64 = base64Url.replace('-', '+').replace('_', '/' ),
            info = JSON.parse($window.atob(base64));
        return info.user;
    };
    storage.setToken = function (token) {

        localStorageService.set('token', token);
    };
    storage.getToken = function () {
        return localStorageService.get('token');
    };
    storage.removeToken = function () {
        localStorageService.remove('token');
        $location.path('/login');
    };

    return storage;
}