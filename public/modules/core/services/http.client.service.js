'use strict';

angular.module('core')
    .service('HttpService', ['$http', '$q', '$log', '$rootScope',
    function ($http, $q, $log, $rootScope) {
        var self = this,
            defaults = {
                params: '',
                data: {},
                transformRequest: null,
                cache: true
            },

            errorStatus = {
                0: 'Request timeout. Please try again.',
                401: 'Unauthorized request. You are not authorized for this request.',
                500: 'Server Error. There is some server error. Please try later.'
            };

        function handleError(response) {
            if (response.status === 0 || response.status === 401 || response.status === 500) {
                $rootScope.connectionErr = true;
                $rootScope.responseError  = errorStatus[response.status];
            }

            if (!angular.isObject(response.data)) {
                return ($q.reject(response));
            }


            // Otherwise, use expected error message.
            return ($q.reject(response));
        }


        // transform the successful response, unwrapping the application data
        // from the API response payload.
        function handleSuccess(response) {
//            $rootScope.gblLoader = false;
            $rootScope.connectionErr = response.status === 0 || response.status === 401;
            return (response);

        }

        // Public API
        /*
         ** Call for POST request
         */
        self.postData = function (url, config) {
            var configure = angular.extend({}, defaults, config);
            //configure.headers['validation-token'] = ipCookie('user')? ipCookie('user').token : '';
            var response = $http({
                method: 'POST',
                url: url,
                data: configure.data,
                headers: configure.headers,
                transformRequest: configure.transformRequest
            });
            return (response.then(handleSuccess, handleError));
        };
        /*
         ** Call for get Request from server
         */
        self.getData = function (url, config) {
            var configure = angular.extend({}, defaults, config);
            //configure.headers['validation-token'] = ipCookie('user')? ipCookie('user').token : '';
            var response = $http.get(url, {
                params: configure.params,
                timeout: 6000,
                headers: configure.headers
            });

            return response.then(handleSuccess, handleError);
        };

        self.putData = function (url, config) {
            var configure = angular.extend({}, defaults, config);
            //configure.headers['validation-token'] = ipCookie('user')? ipCookie('user').token : '';
            var response = $http({
                method: 'PUT',
                url: url,
                data: configure.data,
                headers: configure.headers,
                transformRequest: configure.transformRequest
            });
            return (response.then(handleSuccess, handleError));
        };
        self.delData = function (url, config) {
            var configure = angular.extend({}, defaults, config);
            //configure.headers['validation-token'] = ipCookie('user')? ipCookie('user').token : '';
            var response = $http({
                method: 'DELETE',
                url: url,
                headers: configure.headers
            });
            return (response.then(handleSuccess, handleError));
        };

    }
]);
