'use strict';
/**
 * Module dependencies.
 */
var init = require('./config/init')(),
	config = require('./config/config'),
	mongoose = require('mongoose'),
	chalk = require('chalk');

/**
 * Main application entry file.
 * Please note that the order of loading is important.
 */
global.__base = __dirname + '/';
// Bootstrap db connection
var connectWithRetry = function() {
  return mongoose.connect(config.db, {server: {auto_reconnect: true}} , function(err) {
    if (err) {
      console.error('Failed to connect to mongo on startup - retrying in 5 sec', err);
      setTimeout(connectWithRetry, 5000);
    }
  });
};

var db = connectWithRetry();


// Init the express application
var app = require('./config/express')(db);

var seedHelper = require('./app/utility/seed-data.utility').seedDataHelper;
seedHelper.createSeedData();

// Bootstrap passport config
require('./config/passport')();

// Start the app by listening on <port>
app.listen(config.port);

// Expose app
exports = module.exports = app;

// Logging initialization
console.log('Bonadea Clinic application started on port ' + config.port);